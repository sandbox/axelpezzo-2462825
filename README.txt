INTRODUCTION
------------

Current manteiner: https://www.drupal.org/u/axelpezzo

Wordstatistics report some statistics about the words of the content of the site.

INSTALLING
----------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

After the install you can use this module on this page: admin/reports/wordstatistics

UNINSTALLING
------------

For unistall the module, disable it and uninstall on the modules page or using drush.

ISSUE OR REQUEST
------------

If there are bugs, requests or something else, please report in the issue section on Drupal.org module page. 
